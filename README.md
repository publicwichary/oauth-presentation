﻿# OAuth 2.0 presentation

## OAuthCodeFlowExampleWithIdentityServer 

### How to run it

- Open in Visual Studio 2017 or newer
- Right click on solution and open properties 
- Select multiple startup for all projects to be set to "Start"
- Start debugging
- Four console windows should start
  - API hosted at: http://localhost:5001 (Api.csproj)
  - MVC app hosted at: http://localhost:5002 (Client.csproj) [registered as hybrid flow]
  - Authorization server hosted at: http://localhost:5000 (IdentityServer.csproj)
  - SPA Web Client hosted at: http://localhost:5003 (Web.csproj) [registered as code flow]
- Start MVC app or SPA Web Client and try to authorize
  - username: alice ; password: password
  - username: bob ; password: password



